const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Joi = require("joi");
const userValidationSchema = require("../validations/validations");

const userSchema = new mongoose.Schema({
  fullName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  jti: String,
});

//-----------------------Bcrypting Password before saving in the DB-------------------
userSchema.pre("save", async function (next) {
  const user = this;

  if (!user.isModified("password")) return next();

  try {
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    next();
  } catch (error) {
    return next(error);
  }
});

module.exports = mongoose.model("userDetails", userSchema);
