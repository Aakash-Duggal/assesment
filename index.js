const express = require("express");
require("./conn");

const socialRouter = require("./Routes/routes");

const user = require("./Models/userModel");

const app = express();
app.use(express.json());

app.use("/api/v1/social", socialRouter);

app.listen(5000);
