const user = require("../Models/userModel");
const postModel = require("../Models/postModel");
//-------------------------------------------
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const bcrypt = require("bcryptjs");
const { response } = require("express");
const jwt = require("jsonwebtoken");
const joi = require("joi");
const validations = require("../validations/validations");

//----------------------Registering User----------------------------
exports.postData = async (req, res) => {
  try {
    await validations.validateAsync(req.body);

    const result = await user.create(req.body);

    console.log(result);
    return res.send(result);
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    console.error("Error while saving data:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//-------------------------------Generating Random JTI Here------------------------------------------------
const generateJTI = () => {
  const length = 25;
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let jti = "";

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    jti += characters.charAt(randomIndex);
  }

  return jti;
};

//------------------------------------------Login Check----------------------------------------------------
exports.loginCheck = async (req, res) => {
  try {
    const { email, password } = req.body;

    await validations.emailLoginSchema.validateAsync({ email, password });

    const existingUser = await user.findOne({ email });

    if (!existingUser) {
      return res.status(404).send("User not found");
    }

    // Compairing the provided password with the hashed password in the database using compare method
    const isPasswordValid = await bcrypt.compare(
      password,
      existingUser.password
    );

    if (!isPasswordValid) {
      return res.status(401).send("Invalid password");
    }

    const jti = generateJTI(); //generate JTI function declared above
    const token = jwt.sign(
      { userId: existingUser._id, jti },
      "your-secret-key",
      { expiresIn: "1h" }
    );

    existingUser.jti = jti;
    await existingUser.save(); //saving the jti in the database

    return res.json({ token });
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    console.error("Error during login check:", error);
    return res.status(500).send("Internal Server Error");
  }
};

// function access(header){
// console.log("Header is:",header);
// return
// }

//-----------------------Upload File-------------------------------------
exports.uploadFile = async (req, res) => {
  try {
    const headerData = req.headers;
    // access(headerData);

    console.log(req.file);
    return res.send("Uploaded");
  } catch (error) {
    console.error("Error while Uploading File:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

//----------------------------------Create Post And Like API----------------------------------------------
exports.addImage = async (req, res) => {
  try {
    const { imageUrl, caption, likes,userId } = req.body;
    const newPost = await postModel.create({
      imageUrl,
      caption,
      likes,
      userId,
    });

    console.log("Image added successfully:", newPost);

    return res.status(201).json(newPost);
  } catch (error) {
    console.error("Error while Uploading File:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

//----------------------------------Update Post and Like API-------------------------------------------
