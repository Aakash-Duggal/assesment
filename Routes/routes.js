const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const jwt = require("jsonwebtoken");

//---------------------------Middleware before file upload----------------------------------------------
const verifyTokenMiddleware = (req, res, next) => {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).send("No token provided");
  }

  jwt.verify(token, "your-secret-key", (err, decoded) => {
    if (err) {
      console.error("JWT verification error:", err);
      return res.status(401).send("Unauthorized: Invalid token");
    }
    req.userId = decoded.userId; 
    next();
  });
};

//-----------------multer----------------------------------------------------
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploadedDocuments");
  },

  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
const upload = multer({ storage: storage });

//--------------Route To Register the user----------------------------------
router.route("/register").post(userController.postData);

//--------------Route To Login the User----------------------------------
router.route("/login").post(userController.loginCheck);

//-----------------------File Upload---------------------------------------
router.route("/upload").post(verifyTokenMiddleware,upload.single("file"), userController.uploadFile);

//------------------------Post Create Route----------------------------------------------------
router.route('/postUpload').post(userController.addImage);

module.exports = router;
